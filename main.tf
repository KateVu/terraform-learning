#Configure the AWS Provider
provider "aws" {
  region  = "ap-southeast-2"
  profile = "myprofile"
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
}

variable "subnet_cidr_block" {
  description = "subnet cidir block"
}

variable "environment" {
  description = "deployment environment"
}


resource "aws_vpc" "dev-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "Dev-vpc 192.168.0.0/16"
    EVN  = var.environment
  }
}

resource "aws_subnet" "dev-pub-subnet1" {
  vpc_id     = aws_vpc.dev-vpc.id
  cidr_block = var.subnet_cidr_block
  tags = {
    Name = "192.168.1.0/24 Public subnet ap-southeast-2a"
    EVN  = var.environment
  }
  availability_zone = "ap-southeast-2a"
}

output "dev-vpc-id" {
  value = aws_vpc.dev-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-pub-subnet1.id
}

